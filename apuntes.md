# Areas   
     
**Working area**      
Son los archivos con los que estoy trabajando     
     
**Staging area**      
Son los archivos que agregué en el "add" (después de trabajarlos)     
Los archivos que se agreguen en la staging area son los que al hacer commit se van a desplegar en la commit area     
ej: No se deberian agregar las librerias descargadas por maven al staging area     
**Commit area**   
Una vez realizado el commit se guarda como si fuera una foto.     
Siempre se puede comparar y consultar un commit determinado, a menos que haya modificado cosas de alguna manera sucia.     
Al ultimo commit se lo suele referenciar como _HEAD_   
     
Puedo moverme entre distintos commits con el comando checkout y el número de commit (recuperado de commit log)   
Esto va a generar un estado de detached head mode, en el que los cambios no van a ser guardados a menos que genere un branch nuevo.   
     
**git init**      
Inicia un nuevo proyecto git en dicho directorio     
     
**git remote -v**      
Muestra si el proyecto tiene asignado algun repositorio remoto     
     
**git remote add origin <repositorio https o ssh> **      
Configura la dir del repositorio remoto     
     
     
**git checkout**   
git checkout se utiliza para moverme entre las distintas ramas del proyecto.     
Adicionalmente, si se le agrega la opcion -b, se puede generar una rama nueva en base a donde "esté parado".     


**git status**   
Me muestra el estado del directorio, los archivo que estén en la staging area, los modificados y los que no esté siguiendo.     
Mediante parametros se pueden mostrar más, como por ejemplo aquellos que estén en el .gitignore.  

**git log**    
Muestra el log del projecto git.     
Según las opciones que se utilicen, puede mostrarme commits de la rama en la que me encuentre, o todos los commits del proyecto.   
	--oneline      
	muestra cada commit en una sola linea      
	--decorate      
	muestra los nombres de los commits o branched      
	--graph     
	muestra un grafico     
	--all     
	muestra todo, de todos los branches     
	--stat     
	Detalles de que archivos fueron agregados y eliminados con cada commiy     
	-p     
	muestra mas cosas…       
-n	     
se controla la cantidad de commitIDs que muestra       
--grep=”quick-feature”     
busca todos los commits que tengan esa expresion     
     
git log <directorio>     
muestra el git log de ese directorio     
     
#### Git config   
**git config --global **      
Permite agregar configuraciones      
--list   
Muestra la lista de configuraciones globales     
     
     
me muevo entre branched con “git checkout (master|newbranch|etc..)     
     
**git reset <archivo>**      
Lo remueve de la staging area, y lo vuelve a la modified area     
  
**git reset --hard**   
reinicia todos los cambios hechos en los archivos, volviendo al ultimo commit ( o algo así, revisar)  
  
**git revert HEAD**    
Revierte el proyecto, volviendo al commit anterior al HEAD, pero lo hace haciendo un nuevo commit (es la manera segura de hacer un rollback  

**git reset <commitID>**   
vuelve el proyecto a un commit anterior, removiendo todos los commits que se encuentra arriba del mismo.  
  
**git clean**   
Git clean borra los archivos que no se estén siguiendo dentro del proyecto.

git clean -n    
Muestra que archivos se borarrian, pero sin ejecutar el delete
  
git clean -f   
borra los archivos que no se estén trackeando  
  
git clean -df    
borra los archivos y los directorios que no se estén trackeando  
  
git clean -xf   
borra todos los archivos que no se estén trackeando, incluidos los que se mencionen en el archivo .gitignore  
  
  
  
**git diff <commitID>** me muestra los cambios entre mi working directory y el commitID que escriba. (HEAD es sinónimo para el ultimo commit)  

git diff --staged HEAD   
git diff --cached HEAD   
muestra los cambios entre la staging area y el repositorio  
  
muestra los cambios entre el working directory y la staging area.
  
git diff HEAD-n HEAD   
muestra la diferencia entre dos commits distintos   
se puede poner   
  
git diff HEAD^ HEAD    
git diff HEAD~1 HEAD    
(para comparar el head y el anterior)  
  
git diff -- <archivo>   
compara los cambios para un solo archivo  
  
  
git diff master origin/master   
muestra la diferencia entre mi master y el master del repositorio remoto  
  
git diff feature_branch master   
muestra la diferencia entre la rama feature y la master  
  
  
Git branching    
A git branch represents an independent line of development  
  
git branch -a   
Shows us all the branches

git branch <name>   
Creates a new branch  
  
git checkout -b new-branch-1 master    
Creates the new-branch based on the master branch, and it switch to the new branch  
  
git branch -m small-feature quick-feature   
Will rename the branch small-feature to quick-feature  
  
git branch  -d quick-feature    
Will delete the branch quick-feature  
  
When we create a new branch, that branch inherits all the commits of the based branch  
  
Git merge  
  
One master branch..   
git merge new-branch-1   
Merges my new branch into the master branch   
Usually done after doing a git diff   
Fast forward merge can only happen if there are no changes in the target branch before we do the merge

Now that we done the merge, we don’t need that new branch anymore, so we can delete it


Cuando se hace un merge con la opción --no-ff se conserva la línea del branch que se está mergeando. Resulta en un nuevo commit que es el “merge Commit”

Fast Forward merge is not possible if the project has diverge   
Solo se puede hacer si la rama no cambió entre que se creó el branch y se quiere hacer el merge


3 way merge   
Este merge ocurre cuando hago un branch, lo edito y lo quiero mergear con el master, habiendo sido el master modificado (con otros commits)   
En breve, se realiza igual que otros commits, git tiene un algoritmo recursivo que busca los cambios, pero se debería poder hacer manualmente. --no-ff??   
Se llama 3way commit porque Git utiliza 3 commit para hacer el merge commit. Ellos son los dos HEAD de los branch, y el commit que tienen como ancestro (de donde se generó el branch)

Si se generan conflictos, ya sea porque se tocó el valor de un atributo en dos lugares diferentes, se utiliza alguna herramienta para resolver el merge (git mergetool) y guardados los cambios, se hace un commit.

Luego del commit, git va a guardar una copia del original con una extension .orig   
SI no queremos que se generen, se configura en el cofing de git.   
	git config --global mergetool.keepBackup false

Si no quiero que se trackeen, se agrega *.orig en el gitignore

   
Commit history rewriting in git

git commit --amend --no-edit   
Con la instrucción amend me pisó el último commit con este que estoy haciendo   
Con --no-edit mantiene el mismo mensaje que el commit anterior   
(si no usamos --no-edit, nos pregunta qué mensaje poner al commit)

Sirve más que nada para actualizar algún commit reciente en el que me pudo faltar añadir un archivo en la staging area (ej...algún archivo de configuración que haría que ese commit intermedio rompa…)   
También sirve para re-editar el mensaje del commit si me confundí al tipearlo.

NUNCA hacer un amend de un HEAD que esté en un servidor remoto, en el cual hay gente que pudo haber basado su trabajo.



git rebase   
Es mover una branch a un nuevo commit base

				FEATURE   
			 /-----------(F1)------((F2))   
			/   
-------(C1) ------------(C2) ----------(C3)----------((C4))   
						    |   
						MASTER


Después del rebase:

								FEATURE   
						     /-----------(K1)------((K2))   
						    /   
-------(C1) ------------(C2) ----------(C3)----------((C4))   
						  |   
MASTER

Para hacer esto git modifica la commit history! Por eso es importante tener cuidado, pese a que algunas cosas parezcan iguales…

La manera de hacerlo es, estando en la feature-branch   
git rename <BASE>   
Base puede ser un ID, el nombre de un branch, una etiqueta, o una referencia relativa al HEAD   
ej: git rebase master   
Una de las razones principales para hacer rebase es para tener una historia de proyecto lineal git


Git fetch   
Sincronizes origin master with whatever is in the remote repository   
Origin/Master is the local copy of the remote repository

Despues de sincronizar  origin/master, si tenemos cambios (git log…)  hacemos un merge parados desde el master local...depsués podemos hacer un push si es necesario

Reglas de oro:   
Fetch before you start your day’s work   
Fetch before you push you remote repo   
fetch often - stay in sync as much as possible




Git pull   
Hace un git fetch, seguido de un git merge

pero se puede hacer tipo rebase para hacerlo más limpio   
git pull --rebase <remote name> <branch name>   
Hace más lineal y más limpios los cambios   
Lo que hace es cambiar la base del master local y lo leva a la linea del remoto. Después, aplica a ese nuevo HEAD los cambios que tenemos en nuestro máster local.   
Vendría a ser como que nuestro master local es el branch feature, y que el repositorio remoto es el master local, para cuando haciamos rebase antes…



Git reflog   
Refference logs   
Es la safety net de git   
Es una historia chronologica de todo lo que hice en mi repositorio local   
va de 0 más nuevo a n, más viejo   
se puede acceder al reflog con HEAD@{n} f018e92   
e.g.

git show HEAD@{43}   
commit 85a0c625eb4370f4e33bdbc52097cf10897dcf34   
Author: Guillermo Speicher <guillermo.speicher@gmail.com>   
Date:   Sun Nov 26 16:44:25 2017 -0300

    Estribillo

diff --git a/pokemon.txt b/pokemon.txt   
index 839380f..9810ef5 100644   
--- a/pokemon.txt   
+++ b/pokemon.txt   
@@ -5,3 +5,5 @@ Tengo que ser, siempre el mejor. Mejor que nadie más   
 Atraparlos mi meta es, entrenarlos mi ideal!!!!   
    
 Yo viajaré, de aqui a alla. Buscandolos sin fin..¡¡??   
+   
+Pokeemooon tengo que atraparlos!



Se pueden ver los cambios entre máster y alguno de los reflogs   
tambien se le pueden pasar timestamps para ver la diferencia a nivel horario   
(cuando nos e pasa branch, se asume que es la actual)   
usando git diff @{1.hour.ago}   
git difftool master @{1.hour.ago}


para ver la info formateada   
git log -g master   
reflog es estrictamente local   
la ubicacion está en .git/logs/   
No es parte del repositorio. es más como una lista de undos del repositorio   
no se incluye en pushes, fetches, etc…   
se puede usar el reflog para volver para atrás algún cambio si la cagué mal. (onda, rompí el ultimo commit o borré cosas que no debía.   
Pero hay que tener cuidado, porque los reflog no persisten eternamente

Ej: Hice un reset hard inicial, y quiero volver a los cambios que hice antes…

git checkout REFLOG (antes del reset)   
Ahí entraría en un detatch head state…   
ahí debería git checkout -b rollback-branch   
y depsués puedo hacer un git merge en máster con la rollback

   
Git tagging

Lightweight and annotated   
Son como bookmarks

Lightweight   
Es como una rama que no cambia   
Es un puntero a un commit específico

git tag   
git tag --list   
git tag -l   
Nos muestra todos los tags que generamos.

git tag -l “v-2.1.*”   
git tag --list “v-2.1.*”   
Ejecuta una búsqueda en la lista de etiquetas, y nos devuelve las etiquetas que machean ese resultado


git tag <tag-name>   
Genera un nuevo tag y taguea el HEAD (por defecto)

git tag -a <tag.name>   
Genera una annonated tag   
Annonated tag son más complejas. Pueden tener mensajes, GPG, timestamp, etc

git tag -a <tag.name> -f   
git tag -a <tag.name> --force   
Actualiza una tag de un commit a otro (HEAD actual)



git diff tag1 tag2   
Compara con las etiquetas...

git tag -d <tag-name>   
Elimina una tag

git show <tag-name>   
si quiero ver detalles del tag.   
es lo mismo que hacer git show 375649a (dir del commit)

git checkout tag    
Genera un detached head state

git checkout -b new-branch tag   
Genera un branch a partir de ese tag

git push origin --tags   
Sincroniza todas las tags en el repositorio remoto


git push origin --follow-tags   
Sincroniza las annotated tags al repositorio remoto   
Es mejor solo pushear las annotated tags a los repositorios remotos

git config --global push.followTags true   
Para sincronizar por defecto con los push las annotated tags, hay que configurarlo en .gitconfig


git push origin master :<tag-name-que-quiero-borrar>   
git push origin :tag1 :tag2 :tag3   
Borro tags del repositorio remoto


Como saber si una tag es lightweight o annotated   
git show tag (muestra más info cuando es annotated)

git cat-file -t <tag>   
cat-file muestra contenido, o tipo, o tamaño de objetos del repositorio   
-t define que queremos ver el tipo   
Los tags annotated se muestran como tags, mientras que los lightweight son punteros a los commit



git stash   
Se usa para guardar trabajo temporal para limpiar mi working directory   
Ejemplo:  Estoy trabajando en un branch, y tengo tantas cosas que quiero generar otro branch, pero no quiero perder ni pasar el trabajo que está en el working area (ni hacer un commit y pasar eso)   
WIP = work in progress


git stash    
git stash save   
guarda el working directory y el index state WIP de un branch   
Funciona por defecto en tracked files

git stash save -u    
guarda también archivos que no estén siendo trackeados

git stash save “modified-...”   
Guarda y deja un mensaje

git stash --ket-index   
Guarda en el stash los archivos modificados, pero mantiene los que estén en la staging area

git stash list   
el {0} siempre es el ultimo   
muestra los stach guadados

git stash show stash@{n}   
muestra la informacion del stash (que archivos modifica, etc)

git stash apply   
git va a aplicar el primer stash (ultimo)   
todos los archivos están en modified state (incluso los que estaban en staging area


git stash apply stash@{N}   
se aplica un stash específico

git stash apply --index   
me mantiene en staging area los archivos que ya había pasado allí


git drop   
borra el ultimo stash

git drop  stash@{N}   
borra el stash nro N

git stash pop   
aplica y dropea el último stash

git pop  stash@{N}   
aplica y dropea el stash nro N

git stash clear   
Elimina todos los stashed que puedan haber


git stash branch demobranch   
mueve el stash al nuevo branch   
me redirige al nuevo branch   
Dropea el stash


